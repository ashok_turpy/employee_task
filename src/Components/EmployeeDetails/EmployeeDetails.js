import React from 'react';
import { useLocation } from 'react-router-dom';
import Map from '../Map';

const EmployeeDetails = () => {
    const location = useLocation();

    console.log("Employee Details", location);
    const {
        emp_name = "",
        emp_id = 0,
        emp_address = "",
        emp_department = "",
        emp_designation = "",
        emp_experience = "",
        emp_salary = "",
        emp_photo_path = ""
    } = location.state.employee;

    
    return (
        <div>
            <center>
                {/* <p>EmployeeDetails</p> */}
                <img src={emp_photo_path} alt="employee-img" style={{ borderRadius: '50%', height: '200px', width: '200px' }} />
                <h1>{emp_name}</h1>
                <h6>{emp_designation}</h6><br />
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <div style={{ marginRight: "1em", paddingRight: '2em', borderRight: "1px solid gray" }}>
                        <div style={{ margin: '1rem 0' }}>
                            <h6>Department</h6>
                            <p>{emp_department}</p>
                        </div>

                        <div style={{ margin: '1rem 0' }}>
                            <h6>Employee Experience</h6>
                            <p>{emp_experience}</p>
                        </div>
                    </div>
                    <div style={{ marginLeft: "1em" }}>
                        <div style={{ margin: '1rem 0' }}>
                            <h6>Employee Salary</h6>
                            <p>{emp_salary}</p>
                        </div>

                        <div style={{ margin: '1rem 0' }}>
                            <h6>Employee Id</h6>
                            <p>{emp_id}</p>
                        </div>
                    </div>
                </div><br />
                <div>
                    <h4>{emp_address}</h4>
                </div>

                <iframe
                    src={`https://maps.google.com/maps?q=${emp_address}&t=&z=13&ie=UTF8&iwloc=&output=embed`}
                    frameBorder="0"
                    style={{
                        border: '0',
                        width: '90%',
                        height: '300px',
                        margin: '2em 0'
                    }}
                    allowFullScreen
                    title="map"></iframe>
{/* <Map/> */}
            </center>

        </div>
    )

}

export default EmployeeDetails;