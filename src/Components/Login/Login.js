import React,{useState,useRef} from 'react';
import './Login.css';
import {useNavigate} from 'react-router-dom';


function Login() {

    const[userName, setUserName] = useState("");
    const[password, setPassword] = useState("");

    const nameRef = useRef("");
    const passwordRef = useRef("");

    const navigate = useNavigate();



    const userNameChangeHandler = (e) =>{
        e.preventDefault();
        setUserName(nameRef.current.value);

   }

   const passwordChangeHandler = (e) =>{
    e.preventDefault();
    setPassword(passwordRef.current.value);

}

   const handleSubmit = () =>{
       console.log("username And password",userName,password);
    if(userName === "test" & password === "test"){
        return navigate('/employee-list');
    }

   }

    return (
        <div className="Main">
            <div >
                    <h3>Employee Login</h3><br />
                <div className="form-main">
                    <div className="form-group">
                        <label htmlFor="user-name">Email address</label>
                        <input 
                        type="text" 
                        className="form-control" 
                        id="user-name" 
                        placeholder="Enter email"
                        ref={nameRef} 
                        onChange={userNameChangeHandler} 
                        /><br />
                    </div>
                    <div className="form-group">
                        <label htmlFor="user-password">Password</label>
                        <input type="password" 
                        className="form-control" 
                        id="user-password" 
                        placeholder="Password"
                        ref={passwordRef} 
                        onChange={passwordChangeHandler}
                        /><br />
                    </div>
                    <button 
                    type="submit" 
                    className="btn btn-primary" 
                    onClick={handleSubmit}>Submit</button>
                </div>
            </div>
        </div>
    )
}

export default Login;
