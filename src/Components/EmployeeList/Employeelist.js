import React, { useEffect, useState } from 'react';
import employeeList from '../employee_details';
import EmployeeListAll from './EmployeeListAll';



const EmployeeList = () => {
  const allEmployees = employeeList;

  const [employees, setEmployees] = useState(allEmployees);
  const [selectedDepartment, setSelectedDepartment] = useState('all');
  const [departments, setDepartments] = useState([]);

  const selectedHandler = (e) => {
    setSelectedDepartment(e.target.value);
  }

  console.log("Selected Department", selectedDepartment);

  useEffect(() =>{
      const filteredDepartments = allEmployees.map(item => item.emp_department);
      const departmentsData = [...new Set(filteredDepartments)];
      setDepartments(departmentsData);
  },[]);

  useEffect(() => {
    if(selectedDepartment === "all"){
      setEmployees(allEmployees)
    } else {
      const filterEmployees = allEmployees.filter(employee => employee.emp_department === selectedDepartment);
      console.log(filterEmployees);
      setEmployees(filterEmployees);
    }
  },[selectedDepartment]);

  return (
    <div className="mx-auto" style={{ width: '80%' }}>
      <select name="Departments" id="Departments" onChange={selectedHandler}>
        <option value="none" selected disabled hidden>Select Department</option>
        <option value="all">All Departments</option>
        {departments.map(department => {
          return (
            <option value={department}>{department}</option>
          )})
        }
      </select>

      <EmployeeListAll employees={employees} />

    </div>
  )

}

export default EmployeeList;