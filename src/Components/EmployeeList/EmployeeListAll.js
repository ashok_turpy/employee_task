import React from 'react';
import {useNavigate} from 'react-router-dom';

const EmployeeListAll = ({ employees }) => {

    const navigate = useNavigate();

    const navigateToEmployee = (employee) => {
        console.log("Employee clicked on =>", employee);
        navigate('/employee-details',{state:{employee}});
    }

    return (
        <div>
            <div className="row">
                {employees.map((employee, index) => (
                    <div className="col-lg-3 text-center mt-3" key={index} >
                        <div className="card " onClick={() => navigateToEmployee(employee)} style={{ width: '16rem', display: 'flex', height: '24rem' }} key={index} >
                            <img className="card-img-top" src={employee.emp_photo_path} alt="employee_img" style={{ height: '12rem' }} />
                            <div className="card-main text-center">
                                <div className="card-body">
                                    <h5 className="card-title">{employee.emp_name}</h5>
                                    <p className="card-text">{employee.emp_designation}</p>
                                    <p>{employee.emp_department}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                )
                )}
            </div>
        </div>
    )

}

export default EmployeeListAll;