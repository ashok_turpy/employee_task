
const employeesList= [
    {
        "emp_name":"John",
        "emp_id":"1",
        "emp_address":"328 E 67th St, New York, NY 10065, United States",
        "emp_department":"IT",
        "emp_designation":"software engineer",
        "emp_experience":"4 years",
        "emp_salary":"10,000",
        "emp_photo_path": "/employee_images/1.jpg"
        },

        {
            "emp_name":"Doe",
            "emp_id":"2",
            "emp_address":"200 5th Ave, New York, NY 10010, United States",
            "emp_department":"medical",
            "emp_designation":"doctor",
            "emp_experience":"4 years",
            "emp_salary":"15,000",
            "emp_photo_path": "/employee_images/2.jpg"
        },
        {
            "emp_name":"smith",
            "emp_id":"3",
            "emp_address":"91 Nolan St, Maryborough VIC 3465, Australia",
            "emp_department":"agriculture",
            "emp_designation":"agriculture scientist ",
            "emp_experience":"5 years",
            "emp_salary":"20,000",
            "emp_photo_path": "/employee_images/3.jpg"
        },
        {
            "emp_name":"Steven",
            "emp_id":"4",
            "emp_address":"2350 Calle Tachira, Valle de la Pascua 2350, Guárico, Venezuela",
            "emp_department":"IT",
            "emp_designation":"Associate software Engineer",
            "emp_experience":"1 years",
            "emp_salary":"10,000",
            "emp_photo_path": "/employee_images/4.jpg"
        },
        {
            "emp_name":"kevin",
            "emp_id":"5",
            "emp_address":"71 High St, Sheffield City Centre, Sheffield S1 2GD, United Kingdom",
            "emp_department":"agriculture",
            "emp_designation":"Farm Management",
            "emp_experience":"4 years",
            "emp_salary":"40,000",
            "emp_photo_path": "/employee_images/5.jpg"
        },
        {
            "emp_name":"Paul",
            "emp_id":"6",
            "emp_address":"Kinnaveelish, Co. Galway, Ireland",
            "emp_department":"medical",
            "emp_designation":"M S Ortho",
            "emp_experience":"3 years",
            "emp_salary":"50,000",
            "emp_photo_path": "/employee_images/6.jpg"
        }


];

export default employeesList;