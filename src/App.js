import React from 'react';
import Login from './Components/Login/Login';
import EmployeeList from './Components/EmployeeList/Employeelist';
import EmployeeDetails from './Components/EmployeeDetails/EmployeeDetails';
import { createBrowserHistory } from 'history';
import { Route, Routes} from 'react-router-dom';

function App() {
  return (
    
   <div>
     
     {/* <HashRouter history={createBrowserHistory}> */}
       <div>
          <Routes>
            <Route path="/"  element={<Login />}/>
            <Route path="/employee-list"  element={<EmployeeList />}/>
            <Route path="/employee-details"  element={<EmployeeDetails />}/>
          </Routes>
       </div>
     {/* </HashRouter> */}

   </div>
  );
}

export default App;
